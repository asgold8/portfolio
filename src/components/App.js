import React from 'react';
import {HashRouter, Route} from 'react-router-dom';

import Layout from '../MyLayout';
import About from './About';
import Header from './Header';
import Landing from './Landing';
import Projects from './Projects';


const App = () => {
  return (
    <Layout>
      <div className="ui text container">
        <HashRouter>
        <Header/>
        <h1 className="ui header">Alex G.'s Portfolio</h1>
        <Route path="/" exact component={Landing}/>
        <Route path="/About" exact component={About}/>
        <Route path="/Projects" exact component={Projects}/>        
      </HashRouter>      
      </div>      
    </Layout>
  );
};

export default App;
