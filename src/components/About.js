import React from 'react';

import Layout from '../MyLayout';

const About = () => {
return (
  <Layout>
    <div className="ui text container">
      <span>
        Hello! This portfolio contains a collection of projects I have worked
        on.
      </span>
      <p>Feel free to reach me via email:</p>
      <a href="mailto:asgold8@gmail.com">asgold8@gmail.com</a>
    </div>
  </Layout>
);
};

export default About;