import React from 'react';

const Projects = () => {
  return (
    <div className="ui text">
      <h2>List of Projects:</h2>
      <div>
        <h3>Published websites:</h3>
        <ul>
          <li>
            <a href="https://next-demo.asgold8.now.sh/">Next.js Demo</a>
          </li>
          <li>
            <a href="https://5o0iz.sse.codesandbox.io/">
              Gatsby.js Weatherbit Demo
            </a>
          </li>
        </ul>
        <h3>Repositories:</h3>
        <ul>
          <li>
            <a href="https://codesandbox.io/u/asgold8">CodeSandbox</a>
          </li>
          <li>
            <a href="https://github.com/asgold8/">Github</a>
          </li>
          <li>
            <a href="https://gitlab.com/asgold8/">Gitlab</a>
          </li>
        </ul>
        <h4>Notable Projects:</h4>
        <ul>
          <li>
            <a href="https://codesandbox.io/s/gatsby-weatherbit-test-5o0iz">
              Gatsby.js Weatherbit Demo CodeSandbox
            </a>
            <iframe
              className="ui container"
              src="https://codesandbox.io/embed/gatsby-weatherbit-test-5o0iz?fontsize=12&theme=dark&view=split"
              style={{
                width: "800px",
                height: "300px",
                border: 0,
                borderRadius: "4px",
                overflow: "hidden",
              }}
              title="gatsby-weatherbit-test"
              allow="geolocation; microphone; camera; midi; vr; accelerometer; gyroscope; payment; ambient-light-sensor; encrypted-media; usb"
              sandbox="allow-modals allow-forms allow-popups allow-scripts allow-same-origin"
            ></iframe>
          </li>
          <li>
            <a href="https://gitlab.com/asgold8/next-demo/">
              Next.js Demo Repository
            </a>
          </li>
          <li>
            <a href="https://gitlab.com/asgold8/portfolio/">
              Portfolio Repository
            </a>
          </li>
          <li>
            <a href="https://gitlab.com/asgold8/streaming-demo/">
              Stream Demo App Repository
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
}

export default Projects;